﻿namespace Yugamiru
{
    partial class LanguageSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LanguageSetting));
            this.m_Combo_Language = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ID_OK = new System.Windows.Forms.Button();
            this.ID_Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_Combo_Language
            // 
            this.m_Combo_Language.FormattingEnabled = true;
            this.m_Combo_Language.Items.AddRange(new object[] {
            "English",
            "Japanese",
            "Thai",
            "Korean",
            "Chinese"});
            this.m_Combo_Language.Location = new System.Drawing.Point(215, 33);
            this.m_Combo_Language.Name = "m_Combo_Language";
            this.m_Combo_Language.Size = new System.Drawing.Size(153, 21);
            this.m_Combo_Language.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Please Choose the Language";
            // 
            // ID_OK
            // 
            this.ID_OK.Location = new System.Drawing.Point(84, 80);
            this.ID_OK.Name = "ID_OK";
            this.ID_OK.Size = new System.Drawing.Size(75, 23);
            this.ID_OK.TabIndex = 2;
            this.ID_OK.Text = "OK";
            this.ID_OK.UseVisualStyleBackColor = true;
            this.ID_OK.Click += new System.EventHandler(this.ID_OK_Click);
            // 
            // ID_Cancel
            // 
            this.ID_Cancel.Location = new System.Drawing.Point(215, 80);
            this.ID_Cancel.Name = "ID_Cancel";
            this.ID_Cancel.Size = new System.Drawing.Size(75, 23);
            this.ID_Cancel.TabIndex = 3;
            this.ID_Cancel.Text = "CANCEL";
            this.ID_Cancel.UseVisualStyleBackColor = true;
            this.ID_Cancel.Click += new System.EventHandler(this.ID_Cancel_Click);
            // 
            // LanguageSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 108);
            this.Controls.Add(this.ID_Cancel);
            this.Controls.Add(this.ID_OK);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_Combo_Language);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LanguageSetting";
            this.Text = "LanguageSetting";
            this.Load += new System.EventHandler(this.LanguageSetting_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox m_Combo_Language;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ID_OK;
        private System.Windows.Forms.Button ID_Cancel;
    }
}