﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using static Yugamiru.stretchDIBbits;

namespace Yugamiru
{
    public class ImagePreviewWnd
    {
        // ImagePreviewWnd.cpp : ŽÀ‘•ƒtƒ@ƒCƒ‹
        //
        public byte[] m_pbyteBits;
        // public Bitmap m_bmi;
        public stretchDIBbits.BITMAPINFO m_bmi = new stretchDIBbits.BITMAPINFO();
        public Graphics m_pDCOffscreen;
        public Bitmap m_pbmOffscreen;
        public Bitmap m_pbmOffscreenOld;
        public int m_iOffscreenWidth;
        public int m_iOffscreenHeight;
        public int m_iBackgroundWidth;
        public int m_iBackgroundHeight;


        // CImagePreviewWnd

        //IMPLEMENT_DYNAMIC(CImagePreviewWnd, CWnd)
        public ImagePreviewWnd()
        {

            m_pbyteBits = null;

            m_pDCOffscreen = null;

            m_pbmOffscreen = null;

            m_pbmOffscreenOld = null;

            m_iOffscreenWidth = 0;

            m_iOffscreenHeight = 0;

            m_iBackgroundWidth = 0;

            m_iBackgroundHeight = 0;

            //  memset(&m_bmi, 0, sizeof(m_bmi));
        }

        ~ImagePreviewWnd()
        {
            Cleanup();
        }

        public void Cleanup()
        {
            if (m_pbyteBits != null)
            {
                //delete[] m_pbyteBits;
                m_pbyteBits = null;
            }
            if (m_pDCOffscreen != null)
            {
                if (m_pbmOffscreenOld != null)
                {
                    //m_pDCOffscreen->SelectObject(m_pbmOffscreenOld);
                    m_pbmOffscreenOld = null;
                }
                //delete m_pDCOffscreen;
                m_pDCOffscreen = null;
            }
            if (m_pbmOffscreen != null)
            {
                //delete m_pbmOffscreen;
                m_pbmOffscreen = null;
            }
        }




        // CImagePreviewWnd ƒƒbƒZ[ƒW ƒnƒ“ƒhƒ‰



        public void OnPaint()
        {
            //CPaintDC dc(this); // device context for painting

            Rectangle rcClient;
            //GetClientRect(rcClient);
            /*       if ((m_pDCOffscreen != null) &&
                       (rcClient.Width() == m_iOffscreenWidth) &&
                       (rcClient.Height() == m_iOffscreenHeight))
                   {
                       dc.BitBlt(0, 0, rcClient.Width(), rcClient.Height(), m_pDCOffscreen, 0, 0, SRCCOPY);
                   }
                   else
                   {
                       dc.FillSolidRect(&rcClient, RGB(0, 0, 0));
                   }*/
        }

        public void OnDestroy()
        {
            Cleanup();
            /// CWnd::OnDestroy();
        }

        public void nSize(uint nType, int cx, int cy)
        {
            //CWnd::OnSize(nType, cx, cy);
            if (m_pDCOffscreen != null)
            {
                if ((cx == m_iOffscreenWidth) && (cy == m_iOffscreenHeight))
                {
                    // ƒIƒtƒXƒNƒŠ[ƒ“ƒoƒbƒtƒ@‚ÌÄ¶¬•s—v.
                    return;
                }
                // ƒIƒtƒXƒNƒŠ[ƒ“ƒoƒbƒtƒ@‚ðÄ¶¬.
                if (m_pbmOffscreenOld != null)
                {
                    // m_pDCOffscreen->SelectObject(m_pbmOffscreenOld);
                    m_pbmOffscreenOld = null;
                }
            }
            else
            {
                // ƒIƒtƒXƒNƒŠ[ƒ“ƒoƒbƒtƒ@‚ðV‹K¶¬.
                /* m_pDCOffscreen = new CDC;
                 CDC* pDC = GetDC();
                 m_pDCOffscreen->CreateCompatibleDC(pDC);
                 ReleaseDC(pDC);
                 pDC = NULL;*/
            }

            if (m_pbmOffscreen == null)
            {
                // m_pbmOffscreen = new CBitmap;
            }
            if (m_pbmOffscreen != null)
            {
                //delete m_pbmOffscreen;
                m_pbmOffscreen = null;
            }
            /*    m_pbmOffscreen = new CBitmap;
                CDC* pDC = GetDC();
                m_pbmOffscreen->CreateCompatibleBitmap(pDC, cx, cy);
                ReleaseDC(pDC);
                pDC = NULL;
                m_pbmOffscreenOld = m_pDCOffscreen->SelectObject(m_pbmOffscreen);
                if (m_pbyteBits != NULL)
                {
                    m_pDCOffscreen->SetStretchBltMode(HALFTONE);

            ::StretchDIBits(m_pDCOffscreen->GetSafeHdc(),
                0, 0, cx, cy,
                0,
                (m_iBackgroundHeight - 1 - (m_iBackgroundHeight - 1)),
                m_iBackgroundWidth,
                m_iBackgroundHeight,
                m_pbyteBits, &m_bmi, DIB_RGB_COLORS, SRCCOPY);
                }
                else
                {
                    m_pDCOffscreen->FillSolidRect(0, 0, cx, cy, RGB(0, 0, 0));
                }*/
            m_iOffscreenWidth = cx;
            m_iOffscreenHeight = cy;
        }

        public bool OnEraseBkgnd(Graphics pDC)
        {
            return true;
        }

        public void SetBackgroundBitmap(Image<Bgr, byte> piplimageSrc)
        {
            
            Image<Bgr, Byte> ResizedImage = piplimageSrc.Resize(1024, 1280, Emgu.CV.CvEnum.Inter.Linear);
            m_pbyteBits = new byte[1024 * 1280 * piplimageSrc.MIplImage.NChannels];
            //m_pbyteBits = new byte[piplimageSrc.MIplImage.width * piplimageSrc.MIplImage.height * piplimageSrc.MIplImage.nChannels];
            m_pbyteBits = ResizedImage.Bytes;

             m_bmi.bmiHeader.biSize = 40;//sizeof(BITMAPINFOHEADER);
                m_bmi.bmiHeader.biWidth			= 1024;
                m_bmi.bmiHeader.biHeight		= -1280;
                m_bmi.bmiHeader.biPlanes		= 1;
                m_bmi.bmiHeader.biBitCount		= 24;
                m_bmi.bmiHeader.biCompression	= 0;
                m_bmi.bmiHeader.biSizeImage		= 0;
                m_bmi.bmiHeader.biXPelsPerMeter	= 0;
                m_bmi.bmiHeader.biYPelsPerMeter	= 0;
                m_bmi.bmiHeader.biClrUsed		= 0;
                m_bmi.bmiHeader.biClrImportant	= 0;
                m_bmi.bmiColors = new RGBQUAD[] { new RGBQUAD { } };
                m_bmi.bmiColors[0].rgbBlue		= 255;
                m_bmi.bmiColors[0].rgbGreen		= 255;
                m_bmi.bmiColors[0].rgbRed		= 255;
                m_bmi.bmiColors[0].rgbReserved	= 255;

            //m_bmi = new Bitmap(piplimageSrc.Width, piplimageSrc.Height, PixelFormat.Format24bppRgb);

            m_iBackgroundWidth = 1024;
            m_iBackgroundHeight = 1280;

        }

        public bool SetBackgroundBitmap(int iWidth, int iHeight, byte[] pbyteBits)
        {
            m_bmi.bmiHeader.biSize = 40;//sizeof(BITMAPINFOHEADER);
               m_bmi.bmiHeader.biWidth = iWidth;
               m_bmi.bmiHeader.biHeight = -iHeight;
               m_bmi.bmiHeader.biPlanes = 1;
               m_bmi.bmiHeader.biBitCount = 24;
               m_bmi.bmiHeader.biCompression = 0;
               m_bmi.bmiHeader.biSizeImage = 0;
               m_bmi.bmiHeader.biXPelsPerMeter = 0;
               m_bmi.bmiHeader.biYPelsPerMeter = 0;
               m_bmi.bmiHeader.biClrUsed = 0;
               m_bmi.bmiHeader.biClrImportant = 0;
            m_bmi.bmiColors = new RGBQUAD[] { new RGBQUAD { } };
            m_bmi.bmiColors[0].rgbBlue = 255;
               m_bmi.bmiColors[0].rgbGreen = 255;
               m_bmi.bmiColors[0].rgbRed = 255;
               m_bmi.bmiColors[0].rgbReserved = 255;
            //m_bmi = new Bitmap(iWidth, iHeight, PixelFormat.Format24bppRgb);

            m_iBackgroundWidth = iWidth;
            m_iBackgroundHeight = iHeight;
/*
            int iBmpWidthStep = (iWidth * 3 + 3) / 4 * 4;
            int iBmpBitsSize = iBmpWidthStep * iHeight;
            if (iBmpBitsSize <= 0)
            {
                return false;
            }

            if (m_pbyteBits != null)
            {
                // delete[] m_pbyteBits;
                m_pbyteBits = null;
            }
            m_pbyteBits = new byte[iBmpBitsSize];
            if (m_pbyteBits == null)
            {
                return false;
            }
            int i = 0;*/
            /*  for (i = 0; i < iBmpBitsSize; i++)
              {
                  m_pbyteBits[i] = pbyteBits[i];
              }*/
            m_pbyteBits = pbyteBits;

            return true;
        }

        public bool SetBackgroundBitmap(int iWidth, int iHeight)
        {

            m_bmi.bmiHeader.biSize = 40;//sizeof(BITMAPINFOHEADER);
    m_bmi.bmiHeader.biWidth = iWidth;
    m_bmi.bmiHeader.biHeight = iHeight;
    m_bmi.bmiHeader.biPlanes = 1;
    m_bmi.bmiHeader.biBitCount = 24;
    m_bmi.bmiHeader.biCompression = 0;
    m_bmi.bmiHeader.biSizeImage = 0;
    m_bmi.bmiHeader.biXPelsPerMeter = 0;
    m_bmi.bmiHeader.biYPelsPerMeter = 0;
    m_bmi.bmiHeader.biClrUsed = 0;
    m_bmi.bmiHeader.biClrImportant = 0;

    m_bmi.bmiColors[0].rgbBlue = 255;
    m_bmi.bmiColors[0].rgbGreen = 255;
    m_bmi.bmiColors[0].rgbRed = 255;
    m_bmi.bmiColors[0].rgbReserved = 255;
    

            //m_bmi = new Bitmap(iWidth, iHeight, PixelFormat.Format24bppRgb);
            m_iBackgroundWidth = iWidth;
            m_iBackgroundHeight = iHeight;

            int iBmpWidthStep = (iWidth * 3 + 3) / 4 * 4;
            int iBmpBitsSize = iBmpWidthStep * iHeight;
            if (iBmpBitsSize <= 0)
            {
                return false;
            }

            if (m_pbyteBits != null)
            {
                //delete[] m_pbyteBits;
                m_pbyteBits = null;
            }
            m_pbyteBits = new byte[iBmpBitsSize];
            if (m_pbyteBits == null)
            {
                return false;
            }
            int i = 0;
            for (i = 0; i < iBmpBitsSize; i++)
            {
                m_pbyteBits[i] = 0;
            }

            return true;
        }

        public void UpdateOffscreen(Graphics m_pDCOffscreen)
        {
            if (m_pDCOffscreen == null)
            {
                return;
            }

            if (m_pbyteBits != null)
            {
                //SolidBrush m_Brush = new SolidBrush(Color.FromArgb(0, 0, 0));

                //m_pDCOffscreen->FillSolidRect(0, 0, m_iOffscreenWidth, m_iOffscreenHeight, RGB(0, 0, 0));

                //     m_pDCOffscreen.FillRectangle(m_Brush, 0, 0, m_iOffscreenWidth, m_iOffscreenHeight);
                stretchDIBbits.SetStretchBltMode(m_pDCOffscreen.GetHdc(),StretchBltMode.STRETCH_HALFTONE);
                m_pDCOffscreen.ReleaseHdc();



                stretchDIBbits.StretchDIBits(
                            m_pDCOffscreen.GetHdc(),
                            0,
                            0,
                            m_iOffscreenWidth,
                            m_iOffscreenHeight,
                            0,
                            (m_iBackgroundHeight - 1 - (0 + m_iBackgroundHeight - 1)),
                            m_iBackgroundWidth,
                            m_iBackgroundHeight,
                            m_pbyteBits,
                             ref m_bmi,
                            Constants.DIB_RGB_COLORS,
                            Constants.SRCCOPY);
                m_pDCOffscreen.ReleaseHdc();
            }
           
        }

        public int GetBackgroundWidth()
        {
            return m_iBackgroundWidth;
        }

        public int GetBackgroundHeight()
        {
            return m_iBackgroundHeight;
        }

        public byte[] GetBackgroundBitmap(Graphics hDCRef, byte[] pbyteBits)
        {
            int iBmpWidthStep = (m_iBackgroundWidth * 3 + 3) / 4 * 4;
            int iBmpBitsSize = iBmpWidthStep * m_iBackgroundHeight;
            if (iBmpBitsSize > 0)
            {
                int i = 0;
                for (i = 0; i < iBmpBitsSize; i++)
                {
                    pbyteBits[i] = m_pbyteBits[i];
                }
            }
            return pbyteBits;
        }

    }
}
