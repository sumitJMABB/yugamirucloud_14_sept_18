﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class CCalcPostureProp
    {
        public double HipMko = 0;                      // ƒxƒ‹ƒg‚Ì’[‚©‚çŒÒŠÖß‚Ü‚Å‚Ì“à‘}iƒxƒ‹ƒg•‚É‘Î‚·‚é“j
        public double Belt2KneeRatio = 0;              // ƒxƒ‹ƒg‚Ì‚‚³‚É‘Î‚µ‚ÄA•G‚Ì‚‚³‚Æ‚·‚é”ä—¦
        public double Belt2HipRatio= 0;               // ƒxƒ‹ƒg‚Ì‚‚³‚É‘Î‚µ‚ÄAŒÒŠÖß‚Ì‚‚³‚Æ‚·‚é”ä—¦
        public double KneeRatioError = 0;              // ‹üLŽž‚Ì•G‚ÌˆÊ’u‚Ì”÷’²®—p(—§ˆÊ‚ðŠî€‚Æ‚µ‚Ä)	
        public int [] ShoulderOffset = new int[3];              // Edit‰æ–Ê—p‚ÌŒ¨ƒ}[ƒJ[‚ÌƒIƒtƒZƒbƒg’l

        public  CCalcPostureProp()
        {    
            ShoulderOffset[0] = 0;
            ShoulderOffset[1] = 0;
            ShoulderOffset[2] = 0;
        }

       
~CCalcPostureProp()
{
}



  public   void Initialize(
            double HipMkoNew, double Belt2KneeRatioNew, double Belt2HipRatioNew, double KneeRatioErrorNew,
            int ShoulderOffset0, int ShoulderOffset1, int ShoulderOffset2)
    {
        HipMko = HipMkoNew;
        Belt2KneeRatio = Belt2KneeRatioNew;
        Belt2HipRatio = Belt2HipRatioNew;
        KneeRatioError = KneeRatioErrorNew;
        ShoulderOffset[0] = ShoulderOffset0;
        ShoulderOffset[1] = ShoulderOffset1;
        ShoulderOffset[2] = ShoulderOffset2;
    }

public double GetHipMko() 
{
	return HipMko;
}

public double GetBelt2KneeRatio() 
{
	return Belt2KneeRatio;
}

public double GetBelt2HipRatio() 
{
	return Belt2HipRatio;
}

public double GetKneeRatioError( ) 
{
	return KneeRatioError;
}

public int GetShoulderOffset(int iIndex) 
{
	if ( iIndex< 0 ){
		return 0;
	}
	if ( iIndex >= 3 ){
		return 0;
	}
	return ShoulderOffset[iIndex];
}

public void SetHipMko(double dValue)
{
    HipMko = dValue;
}

public void SetBelt2KneeRatio(double dValue)
{
    Belt2KneeRatio = dValue;
}

public void SetBelt2HipRatio(double dValue)
{
    Belt2HipRatio = dValue;
}

public void SetKneeRatioError(double dValue)
{
    KneeRatioError = dValue;
}

public void SetShoulderOffset(int iIndex, int iValue)
{
    if (iIndex < 0)
    {
        return;
    }
    if (iIndex >= 3)
    {
        return;
    }
    ShoulderOffset[iIndex] = iValue;
}
    }
}
