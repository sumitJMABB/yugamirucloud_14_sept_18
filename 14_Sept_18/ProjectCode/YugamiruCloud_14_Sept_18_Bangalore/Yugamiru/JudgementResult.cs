﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    
    public class JudgementResult
    {
        int m_iPelvicRotate;
        int m_iPelvicTilt;
        int m_iRightLeg;
        int m_iLeftLeg;
        int m_iShoulder;
        int m_iNeck;
        JudgementResult()
        {
            m_iPelvicRotate = 0;

            m_iPelvicTilt = 0;

            m_iRightLeg = 0;

            m_iLeftLeg = 0;

            m_iShoulder = 0;

            m_iNeck = 0;
        
        }

     public   JudgementResult( FrontBodyResultData FrontBodyResultData, SideBodyResultData SideBodyResultData ) 
{
            m_iPelvicRotate = 0;

            m_iPelvicTilt = 0;

            m_iRightLeg = 0;

            m_iLeftLeg = 0;

            m_iShoulder = 0;

            m_iNeck = 0;
        
            SetResultData(FrontBodyResultData, SideBodyResultData);
        }

        public JudgementResult( JudgementResult rSrc )
        {

            m_iPelvicRotate = rSrc.m_iPelvicRotate;

            m_iPelvicTilt = rSrc.m_iPelvicTilt;

            m_iRightLeg = rSrc.m_iRightLeg;

            m_iLeftLeg = rSrc.m_iLeftLeg;

            m_iShoulder = rSrc.m_iShoulder;

            m_iNeck = rSrc.m_iNeck;
        
        }

        

   public int GetPelvicRotate() 
{
	return m_iPelvicRotate;
}

public int GetPelvicTilt() 
{
	return m_iPelvicTilt;
}

public int GetRightLeg() 
{
	return m_iRightLeg;
}

public int GetLeftLeg( ) 
{
	return m_iLeftLeg;
}

public int GetShoulder() 
{
	return m_iShoulder;
}

public int GetNeck() 
{
	return m_iNeck;
}

public void SetResultData( FrontBodyResultData FrontBodyResultData, SideBodyResultData SideBodyResultData )
{
    m_iPelvicRotate = 0;
    if (FrontBodyResultData.GetHip() < 0)
    {
        m_iPelvicRotate = -1;
    }
    else if (FrontBodyResultData.GetHip() > 0)
    {
        m_iPelvicRotate = 1;
    }

    m_iPelvicTilt = 0;
    if (FrontBodyResultData.GetCenterBalance() < 0)
    {
        m_iPelvicTilt = -1;
    }
    else if (FrontBodyResultData.GetCenterBalance() > 0)
    {
        m_iPelvicTilt = 1;
    }

    m_iRightLeg = 0;
    if (FrontBodyResultData.GetRightKnee() < 0)
    {
        m_iRightLeg = -1;
    }
    else if (FrontBodyResultData.GetRightKnee() > 0)
    {
        m_iRightLeg = 1;
    }

    m_iLeftLeg = 0;
    if (FrontBodyResultData.GetLeftKnee() < 0)
    {
        m_iLeftLeg = -1;
    }
    else if (FrontBodyResultData.GetLeftKnee() > 0)
    {
        m_iLeftLeg = 1;
    }

    m_iShoulder = 0;
    if ((FrontBodyResultData.GetShoulderBal() == -2) && ((FrontBodyResultData.GetHeadCenter() == -2) || (FrontBodyResultData.GetEarBal() == -2)))
    {
        m_iShoulder = -2;
    }
    else if (FrontBodyResultData.GetShoulderBal() <= -1)
    {
        m_iShoulder = -1;
    }
    else if ((FrontBodyResultData.GetShoulderBal() == 2) && ((FrontBodyResultData.GetHeadCenter() == 2) || (FrontBodyResultData.GetEarBal() == 2)))
    {
        m_iShoulder = 2;
    }
    else if (FrontBodyResultData.GetShoulderBal() >= 1)
    {
        m_iShoulder = 1;
    }

    m_iNeck = 0;
    if ((FrontBodyResultData.GetHeadCenter() == -2) && (SideBodyResultData.GetNeckForwardLeaning() == 1))
    {
        m_iNeck = -2;
    }
    else if (FrontBodyResultData.GetHeadCenter() <= -1)
    {
        m_iNeck = -1;
    }
    else if ((FrontBodyResultData.GetHeadCenter() == 2) && (SideBodyResultData.GetNeckForwardLeaning() == 1))
    {
        m_iNeck = 2;
    }
    else if (FrontBodyResultData.GetHeadCenter() >= 1)
    {
        m_iNeck = 1;
    }
}


    }
}
